class ReservationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_current_user_reservation, only:[:update, :destroy]

  def create
    reservation_start = reservation_params[:start_reserv]
    reservation_end = reservation_params[:end_reserv]

    diff_minutes = TimeDiffernce.berween(reservation_start, reservation_end).in_minures

    if diff_minutes.to_i % 30 != 0
      render 'tables/show', alert: "Резервация должна быть кратна 30 минутам"
    end

    table_to_reserv = Table.fid(reservation_params[:new_reservation_table_id])
    rest_of_table_to_reserv = table_to_reserv.restaurant

    if rest_of_table_to_reserv.start_work > reservation_start
      render 'tables/show', alert: "Ресторан еще не открыт в это время"
    elsif rest_of_table_to_reserv.end_work < reservation_end
      render 'tables/show', alert: "Ресторан уже закрыт в это время"
    end


    user_reservations_for_time = Reservation.where(user_id: current_user.id).where(start_reserv: reservation_start)
    other_reservations = table_to_reserv.reservations.where.not(user_id: current_user.id)
    user_reservations_for_same_time = table_to_reserv.reservations.where(user_id: current_user.id)

    if user_reservations_for_time.nil?
      @reservation = Reservation.new(reservation_params)
      @reservation.user = set_current_user_reservation

      if @reservation.save
        redirect_to @reservation.table, notice: 'Reservation was successfully created'
      else
        render 'tables/show'
      end
    else
      if !other_reservations.nil?
        for o_reserv in other_reservations do
          if reservation_end.between?(o_reserv.reserv_start, o_reserv.reserv_end) ||
              reservation_start.between?(o_reserv.reserv_start, o_reserv.reserv_end)
            render 'tables/show', alert: "Этот стол уже занят на данный промежуток времени"
          end
        end
      end
      for u_reserv in user_reservations_for_same_time do
        if reservation_end.between?(u_reserv.reserv_start, u_reserv.reserv_end) ||
            reservation_start.between?(u_reserv.reserv_start, u_reserv.reserv_end)
          render 'tables/show', alert: "Вы уже зарезервировали стол на подобное время"
        end
      end
      for u_reserv in user_reservations_for_time do
        if u_reserv.table.restaurant != rest_of_table_to_reserv
          render 'tables/show', alert: 'Нельзя зарезервировать столики в разных ресторанах на одно время'
        elsif u_reserv.table == table_to_reserv
          render 'tables/show', alert: 'Нельзя зарезервировать стол второй раз'

        end
      end
    end


  end

  def update
    if @reservation.update(reservation_params)
      redirect_to @reservation.table, notice: 'Reservation was successfully updated'
    else
      render :edit
    end
  end

  def destroy
    @reservation.destroy
    redirect_to @reservation.table, notice: 'Reservation was successfully destroyed'
  end

  private

    def set_current_user_reservation
      @reservation = current_user.reservations.find(params["id"])
    end

     def reservation_params
       params.require(:reservation).permit(:start_reserv, :end_reserv, :table_id)
     end
end
