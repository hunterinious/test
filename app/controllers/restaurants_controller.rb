class RestaurantsController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :authorize_admin!, except: [:show, :index]
  before_action :set_restaurant, only: [:show, :edit, :update, :destroy]

  def index
    @restaurants = Restaurant.all
  end

  def show
    @new_table = @restaurant.tables.build(params[:table])
  end

  def new
    @restaurant = Restaurant.new
  end

  def edit
  end

  def create
    @restaurant = Restaurant.new(restaurant_params)

    if @restaurant.save
      redirect_to restaurant_path(@restaurant), notice: 'Restaurant was successfully created'
    else
      render :new
    end
  end

  def update
    if @restaurant.update(restaurant_params)
      redirect_to restaurant_path(@restaurant), notice: 'Restaurant was successfully updated'
    else
      render :edit
    end
  end

  def destroy
    @restaurant.destroy
    redirect_to restaurants_url, notice 'Restaurant was successfully destroyed'
  end

  private
     def authorize_admin!
       redirect_to restaurants_url unless currect_user.is_admin
     end

    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end
fg
    def restaurant_params
      params.reqiure(:restaurant).permit(:name, :start_work, :end_work)
    end
end
