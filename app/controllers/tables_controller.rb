class TablesController < ApplicationController
  before_action :authenticate_user!, except: [:show, :index]
  before_action :authorize_admin!, except: [:show, :index]
  before_action :set_table, only: [:show, :edit, :update, :destroy]
  before_action :set_current_restaurant, only: [:new, :destroy]

  def show
    @new_reservation = @table.reservations.build(params[:reservation])
  end

  def new
    @new_table = @restaurant.tables.build(table_params)
  end

  def edit
  end

  def create
    @table = Table.new(table_params)
    if @table.save
      redirect_to @tbale, notice: 'Table was successfulle created'
    else
      render :new
    end
  end

  def update
    if @table.update(table_params)
      redirect_to @table, notice: 'Table was successfully updated'
    else
      render :edit
    end
  end

  def destroy
    @table.destroy
    redirect_to @restaurant, notice: 'Table was successfully destroyed'
  end

  private

    def set_table
      @table = Table.find(params[:id])
    end

    def set_current_restaurant
      @restaurant = @table.restaurant
    end

    def table_params
      params.ereqire
    end

end
