json.extract! controller, :id, :user, :created_at, :updated_at
json.url controller_url(controller, format: :json)
