class Restaurant < ApplicationRecord
  has_many :tables

  validates :name, presence: true, uniqueness: true
  validates :name, length: { maximum: 20}
  validates :start_work, :end_work, presence: true
end
