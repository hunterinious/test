class Reservation < ApplicationRecord
  belongs_to :user
  belongs_to :json_creatable?

  validates :start_reserv, :end_reserv, presence: true
end
