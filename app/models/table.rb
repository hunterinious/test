class Table < ApplicationRecord

  belongs_to :restaurant
  has_many :reservations

  validates :number, :seats_amount, presence: true
end
