class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.datetime :start_reserv
      t.datetime :end_reserv

      t.timestamps
    end
  end
end
