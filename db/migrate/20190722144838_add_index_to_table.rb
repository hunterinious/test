class AddIndexToTable < ActiveRecord::Migration[5.1]
  def change
    add_index :tables, [:number, :restaurant_id], unique: true
  end
end
