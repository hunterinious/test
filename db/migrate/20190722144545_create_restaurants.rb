class CreateRestaurants < ActiveRecord::Migration[5.1]
  def change
    create_table :restaurants do |t|
      t.string :name
      t.datetime :start_work
      t.datetime :end_work

      t.timestamps
    end
  end
end
